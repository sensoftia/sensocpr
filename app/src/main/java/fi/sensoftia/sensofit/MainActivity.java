package fi.sensoftia.sensofit;

import android.os.SystemClock;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.gc.materialdesign.views.ButtonRectangle;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.NonConfigurationInstance;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.ColorRes;
import org.androidannotations.annotations.res.StringRes;

import de.greenrobot.event.EventBus;

@EActivity(R.layout.activity_main)
@OptionsMenu(R.menu.menu_main)
public class MainActivity extends ActionBarActivity {

	private static final int INITIAL_PUSHES_REMAINING = 60;

	private static final float LOWPASS_ALPHA = 0.3f;

	private static final float ACCEL_CHANGE_THRESHOLD_LOW = 0.125f;
	private static final float ACCEL_CHANGE_THRESHOLD_GOOD = 0.150f;
	private static final float ACCEL_CHANGE_THRESHOLD_HIGH = 0.450f;
	private static final float ACCEL_CHANGE_THRESHOLD_RIBS_BROKEN = 0.700f;

	@NonConfigurationInstance
	@Bean
	SensorTag mSensorTag;

	@ViewById(R.id.btn_calibrate)
	ButtonRectangle mBtnCalibrateAndStart;

	@ViewById(R.id.text_push_count)
	TextView mTextPushCount;

	@ViewById(R.id.text_push_force)
	TextView mTextPushForce;

	@ViewById(R.id.text_speed)
	TextView mTextSpeed;

	@ViewById(R.id.text_speed_rating)
	TextView mTextSpeedRating;

	@StringRes(R.string.low)
	String low;

	@StringRes(R.string.good)
	String good;

	@StringRes(R.string.high)
	String high;

	@StringRes(R.string.broken_ribs)
	String brokenRibs;

	@ColorRes(R.color.red)
	int colorRed;

	@ColorRes(R.color.yellow)
	int colorYellow;

	@ColorRes(R.color.green)
	int colorGreen;

	private GyroscopeEvent prevGyro;
	private AccelerometerEvent prevAccel;

	private GyroscopeEvent gyroCalibrationPoint;
	private AccelerometerEvent accelCalibrationPoint;

	private long previousPushTimestampMillis = SystemClock.elapsedRealtime();
	private long startTimestamp;
	private int pushesRemaining = INITIAL_PUSHES_REMAINING;
	private int pushCount;


	private final Runnable mSpeedTextUpdateTask = new Runnable() {
		@Override
		public void run() {
			double pushesPerMin = pushCount * 1.0 / (SystemClock.elapsedRealtime() - startTimestamp) * 1000.0 * 60.0;
			mTextSpeed.setText(String.format("%d /min", (int)pushesPerMin));
			if (pushesPerMin > 110) {
				mTextSpeedRating.setText(high);
				mTextSpeedRating.setTextColor(colorYellow);
			} else if (pushesPerMin < 90) {
				mTextSpeedRating.setText(low);
				mTextSpeedRating.setTextColor(colorYellow);
			} else {
				mTextSpeedRating.setText(good);
				mTextSpeedRating.setTextColor(colorGreen);
			}
			if (pushesRemaining > 0) {
				mTextSpeed.postDelayed(mSpeedTextUpdateTask, 1000);
			}
		}
	};

	@Override
	protected void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@Override
	protected void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		if (isFinishing()) {
			mSensorTag.disconnect();
			mTextSpeed.removeCallbacks(mSpeedTextUpdateTask);
		}
		super.onDestroy();
	}

	@Click(R.id.btn_connect_sensortag)
	void connectSensorTagClicked() {
		mSensorTag.checkIfPairedAndConnect();
	}

	@Click(R.id.btn_calibrate)
	void setCalibrationPoint() {
		//gyroCalibrationPoint = prevGyro;
		accelCalibrationPoint = prevAccel;
		startTimestamp = SystemClock.elapsedRealtime();
		pushesRemaining = INITIAL_PUSHES_REMAINING;
		pushCount = 0;
		mTextSpeed.removeCallbacks(mSpeedTextUpdateTask);
		mTextSpeed.postDelayed(mSpeedTextUpdateTask, 1000);
		Log.i(getClass().getSimpleName(), "Accel calibration point set to: " + accelCalibrationPoint.toString());
		//Log.i(getClass().getSimpleName(), "Gyro calibration point set to: " + gyroCalibrationPoint.toString());
	}

	public void onEventMainThread(GyroscopeEvent event) {
		if (prevGyro == null) {
			prevGyro = event;
		}
		event.doLowPass(prevGyro, LOWPASS_ALPHA);
		prevGyro = event;
		//mTextGyro.setText(event.toString());
	}

	public void onEventMainThread(AccelerometerEvent event) {
		if (prevAccel == null) {
			prevAccel = event;
		}
		event.doLowPass(prevAccel, LOWPASS_ALPHA);
		prevAccel = event;
		//mTextAccel.setText(event.toString());
		mBtnCalibrateAndStart.setVisibility(View.VISIBLE);
		if (pushHappened() && pushesRemaining > 0) {
			previousPushTimestampMillis = SystemClock.elapsedRealtime();
			pushCount++;
			pushesRemaining--;
			mTextPushCount.setText(String.valueOf(pushesRemaining));
			Log.i(getClass().getSimpleName(), "PUSH #" + pushCount + " HAPPENED!");
		}
	}

	private boolean pushHappened() {
		long timestamp = SystemClock.elapsedRealtime();
		if (timestamp - previousPushTimestampMillis < 250) {
			return false;
		}

		if (prevAccel == null || accelCalibrationPoint == null) {
			return false;
		}

		double pushForce = Math.abs(prevAccel.scaledY - accelCalibrationPoint.scaledY);
		if (pushForce > ACCEL_CHANGE_THRESHOLD_RIBS_BROKEN) {
			mTextPushForce.setText(brokenRibs);
			mTextPushForce.setTextColor(colorRed);
			return true;
		} else if (pushForce > ACCEL_CHANGE_THRESHOLD_HIGH) {
			mTextPushForce.setText(high);
			mTextPushForce.setTextColor(colorYellow);
			return true;
		} else if (pushForce > ACCEL_CHANGE_THRESHOLD_GOOD) {
			mTextPushForce.setText(good);
			mTextPushForce.setTextColor(colorGreen);
			return true;
		} else if (pushForce > ACCEL_CHANGE_THRESHOLD_LOW) {
			mTextPushForce.setText(low);
			mTextPushForce.setTextColor(colorYellow);
			return true;
		}

		return false;
	}

}
