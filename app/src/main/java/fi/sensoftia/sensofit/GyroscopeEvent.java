package fi.sensoftia.sensofit;

public class GyroscopeEvent {

	public float x;
	public float y;
	public float z;

	public GyroscopeEvent(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public void doLowPass(GyroscopeEvent prevValue, float alpha) {
		x = alpha * prevValue.x + (1 - alpha) * x;
		y = alpha * prevValue.y + (1 - alpha) * y;
		z = alpha * prevValue.z + (1 - alpha) * z;
		//z = z + alpha * (prevValue.z - z);
	}

	@Override
	public String toString() {
		return String.format("GyroscopeEvent{\n" +
				"x = %+7.3f,\n" +
				"y = %+7.3f,\n" +
				"z = %+7.3f,\n" +
				'}', x, y, z);
	}
}
