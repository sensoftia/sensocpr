package fi.sensoftia.sensofit;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.Trace;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.UiThread.Propagation;
import org.androidannotations.annotations.res.StringRes;



import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import de.greenrobot.event.EventBus;

import static java.util.UUID.fromString;

@EBean
public class SensorTag {

	private static final String TAG = SensorTag.class.getSimpleName();

	private static final UUID GYRO_DATA = fromString("F000AA51-0451-4000-B000-000000000000");
	private static final UUID GYRO_CONF = fromString("F000AA52-0451-4000-B000-000000000000");
	private static final UUID GYRO_PERIOD = fromString("F000AA53-0451-4000-B000-000000000000");

	private static final UUID ACCEL_DATA = fromString("F000AA11-0451-4000-B000-000000000000");
	private static final UUID ACCEL_CONF = fromString("F000AA12-0451-4000-B000-000000000000");
	private static final UUID ACCEL_PERIOD = fromString("F000AA13-0451-4000-B000-000000000000");

	private static final UUID DESC_NOTIFICATION = fromString("00002902-0000-1000-8000-00805f9b34fb");

	private static final byte DEFAULT_PERIOD = 10;
	private static final long BUSY_TRY_INTERVAL_MILLIS = 100;

	@RootContext
	Activity mActivity;

	@StringRes(R.string.bt_disabled_or_not_available)
	String mBtError;

	@StringRes(R.string.sensortag_not_paired)
	String mSensorTagNotPaired;

	private BluetoothGatt mBluetoothGatt;
	private BluetoothDevice mSensorTagDevice;

	private AtomicBoolean mBusy = new AtomicBoolean();

	private AtomicBoolean mConnected = new AtomicBoolean();
	private AtomicBoolean mServicesDiscovered = new AtomicBoolean();
	private AtomicBoolean mGyroscopeEnabled = new AtomicBoolean();
	private AtomicBoolean mAccelerometerEnabled = new AtomicBoolean(true);

	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
			if (newState == BluetoothGatt.STATE_CONNECTED) {
				mConnected.set(true);
				logIt("SensorTag CONNECTED");
				mBluetoothGatt.discoverServices();
			} else if (newState == BluetoothGatt.STATE_DISCONNECTED) {
				logIt("SensorTag DISCONNECTED");
				mBluetoothGatt = null;
			}
		}

		@Override
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {
			if (status == BluetoothGatt.GATT_SUCCESS) {
				mServicesDiscovered.set(true);
				if (mGyroscopeEnabled.get()) { // Gyroscope was enabled before services were discovered
					enableGyroscope(DEFAULT_PERIOD);
				}
				if (mAccelerometerEnabled.get()) { // Accelerometer was enabled before services were discovered
					enableAccelerometer(DEFAULT_PERIOD);
				}
				logIt("Services discovered");
			} else {
				logIt("onServicesDiscovered received: " + status);
			}
		}

		@Override
		public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
			mBusy.set(false);
			if (status == BluetoothGatt.GATT_SUCCESS) {
				logIt("Successfully wrote to " + characteristic.getUuid());
			} else {
				logIt("Characteristic write status: " + status);
			}
		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
			//logIt("onCharacteristicChanged for: " + characteristic.getUuid());
			UUID uuid = characteristic.getUuid();
			if (uuid.equals(GYRO_DATA)) {
				parseAndPostGyroscopeData(characteristic);
			} else if (uuid.equals(ACCEL_DATA)) {
				parseAndPostAccelerometerData(characteristic);
			}
		}

		@Override
		public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
			mBusy.set(false);
			logIt("Descriptor written..");
		}
	};

	public synchronized void checkIfPairedAndConnect() {
		BluetoothManager btManager = (BluetoothManager) mActivity.getSystemService(Context.BLUETOOTH_SERVICE);
		BluetoothAdapter btAdapter = btManager.getAdapter();
		if (btAdapter == null || !btAdapter.isEnabled()) {
			Toast.makeText(mActivity, mBtError, Toast.LENGTH_LONG).show();
		} else {
			Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
			for (BluetoothDevice device : pairedDevices) {
				if ("SensorTag".equalsIgnoreCase(device.getName())) {
					mSensorTagDevice = device;
					connect();
					return;
				}
			}
			Toast.makeText(mActivity, mSensorTagNotPaired, Toast.LENGTH_LONG).show();
		}
	}

	private void connect() {
		if (mSensorTagDevice == null) {
			Log.e(TAG, "No SensorTag available to connect to!");
			return;
		}
		if (mBluetoothGatt != null) {
			Log.e(TAG, "SensorTag already connected!");
			return;
		}

		Log.i(TAG, "Connecting to SensorTag...");
		mBluetoothGatt = mSensorTagDevice.connectGatt(mActivity, true, mGattCallback);
	}

	public synchronized void disconnect() {
		if (mBluetoothGatt != null) {
			mBluetoothGatt.close();
			mBluetoothGatt = null;
			mConnected.set(false);
		}
	}

	private BluetoothGattCharacteristic findCharacteristic(UUID uuid) {
		for (BluetoothGattService service : mBluetoothGatt.getServices()) {
			for (BluetoothGattCharacteristic characteristic : service.getCharacteristics()) {
				if (characteristic.getUuid().equals(uuid)) {
					return characteristic;
				}
			}
		}
		return null;
	}

	@UiThread(propagation = Propagation.REUSE)
	void enableNotification(UUID characteristicUuid) {
		BluetoothGattCharacteristic characteristic = findCharacteristic(characteristicUuid);
		mBluetoothGatt.setCharacteristicNotification(characteristic, true);

		BluetoothGattDescriptor config = characteristic.getDescriptor(DESC_NOTIFICATION);
		queueWriteDescriptor(config, BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
	}

	/**
	 * Sets period to [period * 10] milliseconds (min 100ms).
	 */
	@UiThread(propagation = Propagation.REUSE)
	void setPeriod(UUID periodUuid, byte periodValue) {
		BluetoothGattCharacteristic period = findCharacteristic(periodUuid);
		queueWriteCharacteristic(period, new byte[] { periodValue });
	}

	@Trace
	@UiThread
	void queueWriteDescriptor(BluetoothGattDescriptor descriptor, byte[] value) {
		while (mBusy.get()) {
			try {
				Thread.sleep(BUSY_TRY_INTERVAL_MILLIS);
			} catch (InterruptedException iex) {
				Log.e(TAG, "queueWriteDescriptor interrupted");
				return;
			}
		}
		mBusy.set(true);
		descriptor.setValue(value);
		mBluetoothGatt.writeDescriptor(descriptor);
	}

	@Trace
	@UiThread(propagation = Propagation.REUSE)
	void queueWriteCharacteristic(BluetoothGattCharacteristic characteristic, byte[] value) {
		while (mBusy.get()) {
			try {
				Thread.sleep(BUSY_TRY_INTERVAL_MILLIS);
			} catch (InterruptedException iex) {
				Log.e(TAG, "queueWriteCharacteristic interrupted");
				return;
			}
		}
		mBusy.set(true);
		characteristic.setValue(value);
		mBluetoothGatt.writeCharacteristic(characteristic);
	}

	@UiThread(propagation = Propagation.REUSE)
	void writeConfig(UUID configCharacteristic, byte value) {
		BluetoothGattCharacteristic config = findCharacteristic(configCharacteristic);
		queueWriteCharacteristic(config, new byte[] { value });
	}

	/**
	 * Enables gyroscope. Uprate rate will be
	 */
	@UiThread(propagation = Propagation.REUSE)
	void enableGyroscope(int period) {
		mGyroscopeEnabled.set(true);
		if (!mServicesDiscovered.get()) {
			mBluetoothGatt.discoverServices();
		} else {
			enableNotification(GYRO_DATA);
			setPeriod(GYRO_PERIOD, DEFAULT_PERIOD);
			writeConfig(GYRO_CONF, (byte)7);
		}
	}

	/**
	 * Enables accelerometer. Uprate rate will be [period * 10] milliseconds (min 100ms)
	 */
	@UiThread(propagation = Propagation.REUSE)
	void enableAccelerometer(int period) {
		mAccelerometerEnabled.set(true);
		if (!mServicesDiscovered.get()) {
			mBluetoothGatt.discoverServices();
		} else {
			enableNotification(ACCEL_DATA);
			setPeriod(ACCEL_PERIOD, DEFAULT_PERIOD);
			writeConfig(ACCEL_CONF, (byte)1);
		}
	}

	private void parseAndPostGyroscopeData(BluetoothGattCharacteristic characteristic) {
		// NB: x,y,z has a weird order.
		float y = shortSignedAtOffset(characteristic, 0) * (500f / 65536f) * -1;
		float x = shortSignedAtOffset(characteristic, 2) * (500f / 65536f);
		float z = shortSignedAtOffset(characteristic, 4) * (500f / 65536f);

		EventBus.getDefault().post(new GyroscopeEvent(x, y, z));
	}

	private void parseAndPostAccelerometerData(BluetoothGattCharacteristic characteristic) {
		/*
		* The accelerometer has the range [-2g, 2g] with unit (1/64)g.
		*
		* To convert from unit (1/64)g to unit g we divide by 64.
		*
		* (g = 9.81 m/s^2)
		*
		* The z value is multiplied with -1 to coincide
		* with how we have arbitrarily defined the positive y direction.
		* (illustrated by the apps accelerometer image)
		* */
		int x = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT8, 0);
		int y = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT8, 1);
		int z = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT8, 2) * -1;

		double scaledX = x / 64.0;
		double scaledY = y / 64.0;
		double scaledZ = z / 64.0;

		EventBus.getDefault().post(new AccelerometerEvent(scaledX, scaledY, scaledZ));
	}

	@UiThread(propagation = Propagation.REUSE)
	void logIt(String msg) {
		Log.d(TAG, msg);
	}


	/* ------------------------------------------------------------------------------
	 * COPY PASTED FROM http://processors.wiki.ti.com/index.php/SensorTag_User_Guide
	 * ------------------------------------------------------------------------------ */

	/**
	 * Gyroscope, Magnetometer, Barometer, IR temperature
	 * all store 16 bit two's complement values in the format
	 * LSB MSB.
	 *
	 * This function extracts these 16 bit two's complement values.
	 * */
	private static int shortSignedAtOffset(BluetoothGattCharacteristic c, int offset) {
		int lowerByte = c.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, offset);
		int upperByte = c.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT8, offset + 1); // Note: interpret MSB as signed.

		return (upperByte << 8) + lowerByte;
	}

}
