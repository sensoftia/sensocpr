package fi.sensoftia.sensofit;

public class AccelerometerEvent {

	public double scaledX;
	public double scaledY;
	public double scaledZ;

	public AccelerometerEvent(double scaledX, double scaledY, double scaledZ) {
		this.scaledX = scaledX;
		this.scaledY = scaledY;
		this.scaledZ = scaledZ;
	}

	public void doLowPass(AccelerometerEvent prevValue, float alpha) {
		scaledX = alpha * prevValue.scaledX + (1 - alpha) * scaledX;
		scaledY = alpha * prevValue.scaledY + (1 - alpha) * scaledY;
		scaledZ = alpha * prevValue.scaledZ + (1 - alpha) * scaledZ;
		//scaledZ = scaledZ + alpha * (prevValue.scaledZ - scaledZ);
	}

	@Override
	public String toString() {
		return String.format("AccelerometerEvent{\n" +
				"scaledX = %+7.3f g,\n" +
				"scaledY = %+7.3f g,\n" +
				"scaledZ = %+7.3f g\n" +
				"}", scaledX, scaledY, scaledZ);
	}
}
